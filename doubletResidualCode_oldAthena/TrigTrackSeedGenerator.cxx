#include <cmath>
#include <iostream>
#include <list>
#include <algorithm>
#include <iterator>
#include "TrigInDetEvent/TrigSiSpacePointBase.h"
#include "TrigInDetPattRecoEvent/TrigInDetTriplet.h"
#include "TrigInDetPattRecoTools/TrigTrackSeedGenerator.h"
#include "IRegionSelector/IRoiDescriptor.h"
#include "IRegionSelector/RoiUtil.h"



// CK tau doublet stuff
bool TrigTrackSeedGenerator::checkClustsInTrack(const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>& clusts, std::vector<Trk::TrkBaseNode*>& trackNodes) {
  
  bool firstClustInTrack = false;
  bool secondClustInTrack = (clusts.second == NULL); // Do not care about second cluster in pixel layers

  std::vector<Trk::TrkBaseNode*>::iterator pnIt(trackNodes.begin()),
    pnEnd(trackNodes.end());
  
  for (;pnIt!=pnEnd; ++pnIt) {    
    if (clusts.first->identify() == (*pnIt)->m_getPrepRawData()->identify())
      firstClustInTrack = true;

    if (clusts.second != NULL) {
      if (clusts.second->identify() == (*pnIt)->m_getPrepRawData()->identify())
	secondClustInTrack = true;
    }

    if (firstClustInTrack && secondClustInTrack) break;
  }
  
  return firstClustInTrack && secondClustInTrack;
}

void TrigTrackSeedGenerator::calcDoubStudyParams(DoubStudyOutParams& dsOutPars, const TrigSiSpacePointBase* midSpacePoint, const TrigSiSpacePointBase* spacePoint, std::vector<Trk::TrkBaseNode*> trackNodes, double z0, double tau, double minZ, double maxZ, double minZ_atSp, double maxZ_atSp, int doubType, bool doubKept) {
    
  const Trk::SpacePoint* offSp = spacePoint->offlineSpacePoint();
  const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*> spClusts = offSp->clusterList();
		  
  bool spInTrack = false;
  spInTrack = checkClustsInTrack(spClusts, trackNodes);

  bool spOutOfRange = false;
  if ( spacePoint->z() < minZ_atSp || spacePoint->z() > maxZ_atSp ) {
    spOutOfRange = true;
  }
		
  if ( spInTrack ) { 		      
    dsOutPars.m_doubZRes->push_back( z0 - m_settings.roiDescriptor->zed() );
    dsOutPars.m_doubTau->push_back( tau );
    // dsOutPars.m_doubEta = eta;
    dsOutPars.m_doubEta->push_back( spacePoint->eta() );
    dsOutPars.m_doubPhi->push_back( spacePoint->phi() );
    dsOutPars.m_doubZed->push_back( spacePoint->z() );
    dsOutPars.m_doubZedZero->push_back( z0 );
    dsOutPars.m_doubRangeMinZ->push_back( minZ );
    dsOutPars.m_doubRangeMaxZ->push_back( maxZ );
    dsOutPars.m_doubRangeAtSpMinZ->push_back( minZ_atSp );
    dsOutPars.m_doubRangeAtSpMaxZ->push_back( maxZ_atSp );
    dsOutPars.m_doubType->push_back( doubType );
    dsOutPars.m_doubLayerDiff->push_back( midSpacePoint->layer() - spacePoint->layer() );
    // dsOutPars.m_doubKept = RoiUtil::contains( *(m_settings.roiDescriptor), z0, tau);
    dsOutPars.m_doubKept->push_back( doubKept );
    dsOutPars.m_doubSpOutOfRange->push_back( spOutOfRange );

    std::vector< double > spZ, spR;

    spZ.push_back( midSpacePoint->z() );
    spZ.push_back( spacePoint->z() );

    spR.push_back( midSpacePoint->r() );
    spR.push_back( spacePoint->r() );

    dsOutPars.m_doubSpZ->push_back( spZ );
    dsOutPars.m_doubSpR->push_back( spR );
    
    std::vector< char > spAreIBL, spArePix, spAreSCT;
    std::vector< long > spLayer;

    spAreIBL.push_back( midSpacePoint->isPixel() && (midSpacePoint->layer() == 0) );
    spAreIBL.push_back( midSpacePoint->isPixel() && (spacePoint->layer() == 0) );
    
    spArePix.push_back( midSpacePoint->isPixel() );
    spArePix.push_back( spacePoint->isPixel() );
    
    spAreSCT.push_back( midSpacePoint->isSCT() );
    spAreSCT.push_back( spacePoint->isSCT() );

    spLayer.push_back( midSpacePoint->layer() );
    spLayer.push_back( spacePoint->layer() );

    dsOutPars.m_doubIsIBL->push_back( spAreIBL );
    dsOutPars.m_doubIsPix->push_back( spArePix );
    dsOutPars.m_doubIsSCT->push_back( spAreSCT );
    dsOutPars.m_doubLayer->push_back( spLayer );


    // Count number of doublets found to be on track
    if (!doubType) ++dsOutPars.m_midNInnerOnTrack;
    else if (doubType) ++dsOutPars.m_midNOuterOnTrack;
  }
}

void TrigTrackSeedGenerator::doDoubStudyReadout(DoubStudyOutParams& dsOutPars, TTree* doubTree, Trk::Track* seedTrack, ULong64_t FTFrunItr){
  // std::cout << "CK TEST: In doDoubStudyReadout" << std::endl; // CK TEST
  
  const DataVector<const Trk::TrackParameters>* trackParams = seedTrack->trackParameters();

  dsOutPars.m_trkD0 = seedTrack->perigeeParameters()->parameters()[Trk::d0];
  dsOutPars.m_trkQoverP = seedTrack->perigeeParameters()->parameters()[Trk::qOverP];
  // dsOutPars.m_trkEta = -std::log( std::tan( seedTrack->perigeeParameters()->parameters()[Trk::theta] / 2 ) );
  dsOutPars.m_trkPt  = trackParams->at(0)->pT();
  dsOutPars.m_trkEta = trackParams->at(0)->eta();
  dsOutPars.m_trkPhi = seedTrack->perigeeParameters()->parameters()[Trk::phi0];
  dsOutPars.m_trkZedZero = seedTrack->perigeeParameters()->parameters()[Trk::z0];
		    
  dsOutPars.m_roiEta = m_settings.roiDescriptor->eta();
  dsOutPars.m_roiPhi = m_settings.roiDescriptor->phi();
  dsOutPars.m_roiZed = m_settings.roiDescriptor->zed();

  // Loop over doublets (using doubZRes as a convenient vector to get length from)
  for (unsigned int doubItr = 0; doubItr < dsOutPars.m_doubZRes->size(); ++doubItr) {
    std::cout << "CK vars: "
  	      << " FTFrunItr " << FTFrunItr
  	      << " roiEta " << dsOutPars.m_roiEta
  	      << " roiPhi " << dsOutPars.m_roiPhi
  	      << " roiZ0 " << dsOutPars.m_roiZed
  	      << " trkEta " << dsOutPars.m_trkEta
  	      << " trkPhi " << dsOutPars.m_trkPhi
  	      << " trkZ0 " << dsOutPars.m_trkZedZero
  	      << " trkPt " << dsOutPars.m_trkPt
  	      << " trkD0 " << dsOutPars.m_trkD0
  	      << " trkQoverP " << dsOutPars.m_trkQoverP
	      << " midspNInnerDoubOnTrack " << dsOutPars.m_midNInnerOnTrack
              << " midspNOuterDoubOnTrack " << dsOutPars.m_midNOuterOnTrack
  	      << " doubEta " << dsOutPars.m_doubEta->at(doubItr)
  	      << " doubPhi " << dsOutPars.m_doubPhi->at(doubItr)
  	      << " doubZed " << dsOutPars.m_doubZed->at(doubItr)
  	      << " doubZ0 " << dsOutPars.m_doubZedZero->at(doubItr)
  	      << " doubZ0Res " << dsOutPars.m_doubZRes->at(doubItr)
              << " doubTau " << dsOutPars.m_doubTau->at(doubItr)
  	      << " doubType " << dsOutPars.m_doubType->at(doubItr)
              << " doubLayerDiff " << dsOutPars.m_doubLayerDiff->at(doubItr)
  	      << " doubKept " << dsOutPars.m_doubKept->at(doubItr)
  	      << std::endl;
  }
  // Commenting this out to make sure you're not printing the phi-r slices where you can't find doublets from the track
  // WARNING: If you have a track which you filter all other spacepoints out from z width extrapolation
  // once you've picked your midpoint sp, this track will not be printed
  
  // if ( dsOutPars.m_doubZRes->size() == 0 ) { // If no doublets were found on track, print default valued info for doublets    
  //   std::cout << "CK vars: "
  //   	      << " FTFrunItr " << FTFrunItr
  //   	      << " roiEta " << dsOutPars.m_roiEta
  //   	      << " roiPhi " << dsOutPars.m_roiPhi
  //   	      << " roiZ0 " << dsOutPars.m_roiZed
  //   	      << " trkEta " << dsOutPars.m_trkEta
  //   	      << " trkPhi " << dsOutPars.m_trkPhi
  //   	      << " trkZ0 " << dsOutPars.m_trkZedZero
  //   	      << " trkPt " << dsOutPars.m_trkPt
  //   	      << " trkD0 " << dsOutPars.m_trkD0
  //   	      << " trkQoverP " << dsOutPars.m_trkQoverP
  //   	      << " midspNInnerDoubOnTrack " << dsOutPars.m_midNInnerOnTrack
  //             << " midspNOuterDoubOnTrack " << dsOutPars.m_midNOuterOnTrack
  //   	      << " doubEta " << -999
  //   	      << " doubPhi " << -999
  //   	      << " doubZed " << -999
  //   	      << " doubZ0 " << -999
  //   	      << " doubZ0Res " << -999
  //   	      << " doubType " << -999
  //   	      << " doubKept " << -999
  //   	      << std::endl;
  // }

  // Making sure you're not printing the phi-r slices where you can't find doublets from the track
  // WARNING: If you have a track which you filter all other spacepoints out from z width extrapolation
  // once you've picked your midpoint sp, this track will not be printed
  if ( dsOutPars.m_doubZRes->size() != 0 ) {
    // std:: cout << "CK TEST: filling doublet tree" << std::endl; // CK TEST
    doubTree->Fill();
  }
    
  // Clearing output params for next midpoint in loop over midpoint space points
  // std:: cout << "CK TEST: Clearing output params at end of current midpoint sp" << std::endl; // CK TEST
  dsOutPars.m_doubZRes->clear();
  dsOutPars.m_doubTau->clear();
  dsOutPars.m_doubEta->clear();
  dsOutPars.m_doubPhi->clear();
  dsOutPars.m_doubZed->clear();
  dsOutPars.m_doubZedZero->clear();
  dsOutPars.m_doubRangeMinZ->clear();
  dsOutPars.m_doubRangeMaxZ->clear();
  dsOutPars.m_doubRangeAtSpMinZ->clear();
  dsOutPars.m_doubRangeAtSpMaxZ->clear();
  dsOutPars.m_doubType->clear();
  dsOutPars.m_doubLayerDiff->clear();
  dsOutPars.m_doubKept->clear();
  dsOutPars.m_doubSpOutOfRange->clear();

  dsOutPars.m_doubSpZ->clear();
  dsOutPars.m_doubSpR->clear();
  
  // m_doubOnTrack->clear();
  dsOutPars.m_doubIsIBL->clear();
  dsOutPars.m_doubIsPix->clear();
  dsOutPars.m_doubIsSCT->clear();
  dsOutPars.m_doubLayer->clear();

  dsOutPars.m_midNInnerOnTrack = 0;
  dsOutPars.m_midNOuterOnTrack = 0;
}

//




TrigTrackSeedGenerator::TrigTrackSeedGenerator(const TrigCombinatorialSettings& tcs) 
  : m_settings(tcs), 
    m_maxSoaSize(1000),
    m_maxOuterRadius(550.0), 
    m_minRadius(10.0),
    m_maxRadius(600.0), 
    m_minDeltaRadius(10.0), 
    m_zTol(3.0), 
    m_pStore(NULL)
{
  // std::cout << "CK TEST: In constructor" << std::endl; // CK TEST
  
  m_maxDeltaRadius = m_settings.m_doublet_dR_Max;
  m_radBinWidth = m_settings.m_seedRadBinWidth;
  m_phiSliceWidth = 2*M_PI/m_settings.m_nMaxPhiSlice;
  m_nMaxRadBin = 1+(int)((m_maxRadius-m_minRadius)/m_radBinWidth);
  m_pStore = new PHI_R_STORAGE(m_settings.m_nMaxPhiSlice, m_nMaxRadBin);

  //mult scatt. variance for doublet matching
  const double radLen = 0.036;
  m_CovMS = std::pow((13.6/m_settings.m_tripletPtMin),2)*radLen;
  const double ptCoeff = 0.29997*1.9972/2.0;// ~0.3*B/2 - assumes nominal field of 2*T
  m_minR_squ = m_settings.m_tripletPtMin*m_settings.m_tripletPtMin/std::pow(ptCoeff,2);
}

TrigTrackSeedGenerator::~TrigTrackSeedGenerator() {  
  m_triplets.clear();
  delete m_pStore;
}

void TrigTrackSeedGenerator::loadSpacePoints(const std::vector<TrigSiSpacePointBase>& vSP) {

  m_pStore->reset();

  for(std::vector<TrigSiSpacePointBase>::const_iterator it = vSP.begin();it != vSP.end();++it) {
    if((*it).r()>m_maxRadius || (*it).r() < m_minRadius) continue;
    int rIdx = ((*it).r()-m_minRadius)/m_radBinWidth;
    int phiIdx = ((*it).phi()+M_PI)/m_phiSliceWidth;
    if (phiIdx >= m_settings.m_nMaxPhiSlice) {
      phiIdx %= m_settings.m_nMaxPhiSlice;
    }
    else if (phiIdx < 0) {
      phiIdx += m_settings.m_nMaxPhiSlice;
      phiIdx %= m_settings.m_nMaxPhiSlice;
    }
      
    m_pStore->addSpacePoint(phiIdx, rIdx, &(*it));
  }
  m_pStore->sortSpacePoints();

  m_SoA.reserveSpacePoints(vSP.size());
}

// void TrigTrackSeedGenerator::createSeeds() {
void TrigTrackSeedGenerator::createSeeds(const std::string algName, std::vector<const TrackCollection*> firstStageTracks, TTree* doubTree, ULong64_t FTFrunItr) { // CK

  // CK STUFF
  DoubStudyOutParams dsOutPars;

  if (algName.compare("TrigFastTrackFinder_TauIso") == 0) {
    doubTree->SetBranchAddress("FTF_runIteration", &FTFrunItr);
    
    doubTree->SetBranchAddress("doub_z0res", &dsOutPars.m_doubZRes);
    doubTree->SetBranchAddress("doub_tau", &dsOutPars.m_doubTau);
    doubTree->SetBranchAddress("doub_eta", &dsOutPars.m_doubEta);
    doubTree->SetBranchAddress("doub_phi", &dsOutPars.m_doubPhi);
    doubTree->SetBranchAddress("doub_z", &dsOutPars.m_doubZed);
    doubTree->SetBranchAddress("doub_z0", &dsOutPars.m_doubZedZero);
    doubTree->SetBranchAddress("doub_range_minZ", &dsOutPars.m_doubRangeMinZ);
    doubTree->SetBranchAddress("doub_range_maxZ", &dsOutPars.m_doubRangeMaxZ);
    doubTree->SetBranchAddress("doub_rangeAtSp_minZ", &dsOutPars.m_doubRangeAtSpMinZ);
    doubTree->SetBranchAddress("doub_rangeAtSp_maxZ", &dsOutPars.m_doubRangeAtSpMaxZ);
    doubTree->SetBranchAddress("doub_type", &dsOutPars.m_doubType);
    doubTree->SetBranchAddress("doub_layerDiff", &dsOutPars.m_doubLayerDiff);
    doubTree->SetBranchAddress("doub_kept", &dsOutPars.m_doubKept);
    doubTree->SetBranchAddress("doub_spOutOfRange", &dsOutPars.m_doubSpOutOfRange);

    doubTree->SetBranchAddress("doub_sp_z", &dsOutPars.m_doubSpZ);
    doubTree->SetBranchAddress("doub_sp_r", &dsOutPars.m_doubSpR);
    
    // doubTree->SetBranchAddress("doub_sp_IsIBL", &dsOutPars.m_doubIsIBL);
    doubTree->SetBranchAddress("doub_sp_IsPix", &dsOutPars.m_doubIsPix);
    doubTree->SetBranchAddress("doub_sp_IsSCT", &dsOutPars.m_doubIsSCT);
    doubTree->SetBranchAddress("doub_sp_Layer", &dsOutPars.m_doubLayer);

    doubTree->SetBranchAddress("midsp_nInnerDoubOnTrack", &dsOutPars.m_midNInnerOnTrack);
    doubTree->SetBranchAddress("midsp_nOuterDoubOnTrack", &dsOutPars.m_midNOuterOnTrack);
    
    doubTree->SetBranchAddress("track_d0", &dsOutPars.m_trkD0);
    doubTree->SetBranchAddress("track_qOverP", &dsOutPars.m_trkQoverP);
    doubTree->SetBranchAddress("track_pT", &dsOutPars.m_trkPt);
    doubTree->SetBranchAddress("track_eta", &dsOutPars.m_trkEta);
    doubTree->SetBranchAddress("track_phi", &dsOutPars.m_trkPhi);
    doubTree->SetBranchAddress("track_z0", &dsOutPars.m_trkZedZero);
    
    doubTree->SetBranchAddress("roi_eta", &dsOutPars.m_roiEta);
    doubTree->SetBranchAddress("roi_phi", &dsOutPars.m_roiPhi);
    doubTree->SetBranchAddress("roi_z0", &dsOutPars.m_roiZed);
  }
  
  // std::cout << "CK TEST: Before making trackMaker" << std::endl;// CK TEST
  ToolHandle<ITrigDkfTrackMakerTool> trackMaker("TrigDkfTrackMakerTool");
  StatusCode sc;// = AthAlgTool::initialize();

  // std::cout << "CK TEST: Before doing trackMaker.retrieve()" << std::endl;// CK TEST
  sc = trackMaker.retrieve();
  sc.isSuccess(); // Checking status code as athena complains if it is unchecked

  Trk::Track* seedTrack = NULL;

  if (algName.compare("TrigFastTrackFinder_TauIso") == 0) {
    // std::cout << "CK TEST: Before retrieving (any) first stage tracks" << std::endl;// CK TEST
    
    // Get last added track collection (i.e. first-stage tracks for this event)
    const TrackCollection* foundTracks = firstStageTracks.back();

    // std::cout << "CK TEST: foundTracks.size() = " << foundTracks->size() << std::endl;// CK TEST

    TrackCollection::const_iterator it = foundTracks->begin();
    TrackCollection::const_iterator itEnd = foundTracks->end();

    // std::cout << "CK TEST: seedTrack before checking for seed track = " << seedTrack << std::endl;// CK TEST
    
    // std::cout << "CK TEST: Before trying to retrieve seed track" << std::endl;// CK TEST
    for (;it!=itEnd;it++){
      if ( std::fabs((*it)->perigeeParameters()->parameters()[Trk::z0] - m_settings.roiDescriptor->zed()) < 1e-3) {
	// std::cout << "CK TEST: Seed track retreived" << std::endl;// CK TEST
	seedTrack = *it;
      }
    }

    // std::cout << "CK TEST: seedTrack after checking for seed track = " << seedTrack << std::endl;// CK TEST
    
    // std::cout << "CK TEST: After trying to retrieve seed track" << std::endl;// CK TEST
  }
  
  std::vector<Trk::TrkBaseNode*> vpTrkNodes;
  bool trackResult;
  // std::cout << "CK TEST: Before trying to createDkfTrack" << std::endl;// CK TEST
  if ( algName.compare("TrigFastTrackFinder_TauIso") == 0 && seedTrack != NULL) {
    // std::cout << "CK TEST: seedTrack before createDkfTrack = " << seedTrack << std::endl;// CK TEST
    
    trackResult = trackMaker->createDkfTrack(*seedTrack,vpTrkNodes, 0.);

    // std::cout << "CK TEST: seedTrack after createDkfTrack = " << seedTrack << std::endl;// CK TEST
  }
  // std::cout << "CK TEST: After trying to createDkfTrack" << std::endl;// CK TEST
  //

  float zMinus = m_settings.roiDescriptor->zedMinus() - m_zTol;
  float zPlus  = m_settings.roiDescriptor->zedPlus() + m_zTol;
  m_triplets.clear();

  for(int phiIdx=0;phiIdx<m_settings.m_nMaxPhiSlice;phiIdx++) {

    const PHI_SECTOR& S = m_pStore->m_phiSectors[phiIdx];

    if(S.m_nSP==0) continue;

    std::vector<int> phiVec;
    phiVec.reserve(4);

    phiVec.push_back(phiIdx);

    if(phiIdx>0) phiVec.push_back(phiIdx-1);
    else phiVec.push_back(m_settings.m_nMaxPhiSlice-1);

    if(phiIdx<m_settings.m_nMaxPhiSlice-1) phiVec.push_back(phiIdx+1);
    else phiVec.push_back(0);
    
    //Remove duplicates
    std::sort(phiVec.begin(), phiVec.end());
    phiVec.erase(std::unique(phiVec.begin(), phiVec.end()), phiVec.end());

    // std::cout << "CK TEST: m_settings.m_seedRadBinWidth = " << m_settings.m_seedRadBinWidth << std::endl; // CK TEST
    // std::cout << "CK TEST: m_radBinWidth = " << m_radBinWidth << std::endl; // CK TEST

    for(int radIdx=0;radIdx<m_nMaxRadBin;radIdx++) {

      if(S.m_radBins[radIdx].empty()) continue;

      for(auto spm : S.m_radBins[radIdx]) {//loop over middle spacepoint

        float zm = spm->z();
        float rm = spm->r();
        bool isSct = (spm->isSCT());

        const std::pair<IdentifierHash, IdentifierHash>& deIds = spm->offlineSpacePoint()->elementIdList();
	const std::pair<const Trk::PrepRawData*, const Trk::PrepRawData*>& deClusts = spm->offlineSpacePoint()->clusterList(); // CK

	// CK

	bool spMidInTrack = false;
	if (algName.compare("TrigFastTrackFinder_TauIso") == 0) {
	  // std::cout << "CK TEST: Before checking middle space point clusts in track" << std::endl;// CK TEST
	  spMidInTrack = checkClustsInTrack(deClusts, vpTrkNodes);
	  // std::cout << "CK TEST: spMidInTrack = " << spMidInTrack << std::endl;// CK TEST
	}
	//
	
        int nSP=0;
        int nInner=0;
        int nOuter=0;

        for(int innRadIdx=radIdx-1;innRadIdx>=0;innRadIdx--) {	  
          float refRad = m_minRadius +  m_radBinWidth*(0.5+innRadIdx);

          float dR = rm - refRad;

          if(dR<m_minDeltaRadius) continue;
          if(dR>m_maxDeltaRadius) break;

          float minZ = zMinus + refRad*(zm - zMinus)/rm;
          float maxZ = zPlus + refRad*(zm - zPlus)/rm;

          for(auto innPhiIdx : phiVec) {

            const std::vector<const TrigSiSpacePointBase*>& spVec = m_pStore->m_phiSectors[innPhiIdx].m_radBins[innRadIdx];

            if(spVec.empty()) continue;

            std::vector<const TrigSiSpacePointBase*>::const_iterator it1 = std::lower_bound(spVec.begin(), spVec.end(), minZ, PHI_SECTOR::smallerThanZ());
            std::vector<const TrigSiSpacePointBase*>::const_iterator it2 = std::upper_bound(spVec.begin(), spVec.end(), maxZ, PHI_SECTOR::greaterThanZ());

            if(std::distance(it1, it2)==0) continue;

            for(std::vector<const TrigSiSpacePointBase*>::const_iterator spIt= it1; spIt!=it2; ++spIt) {

	      // CK comment: this checks if your middle space point and other space point are the same
              if(deIds == (*spIt)->offlineSpacePoint()->elementIdList()) continue;

	      // CK: don't allow doublet formed from space points in the same layer
	      if(spm->layer() == (*spIt)->layer()) continue; // CK

              if (!m_settings.m_tripletDoPSS) {//Allow PSS seeds?
                if(isSct && ((*spIt)->isPixel())) continue;
              }

              double tau = (zm - (*spIt)->z()) / (rm - (*spIt)->r());
              if (fabs(tau) > 7.41) {
                continue;
              }
              double z0  = zm - rm*tau;
              if (m_settings.m_doubletFilterRZ) {
		// std::cout << "CK TEST: Before check for sec stage RoI" << std::endl; // CK TEST

		if ( m_settings.roiDescriptor->zed() !=0 && algName.compare("TrigFastTrackFinder_TauIso") == 0 && spMidInTrack) {
		  // std::cout << "CK TEST: Before doing doDoubStudyReadout for inner doublets" << std::endl; // CK TEST
		  float minZ_atSpIt = zMinus + ( (*spIt)->r() )*(zm - zMinus)/rm;
		  float maxZ_atSpIt = zPlus + ( (*spIt)->r() )*(zm - zPlus)/rm;
		  
		  bool doubIsKept = RoiUtil::contains( *(m_settings.roiDescriptor), z0, tau);
		  // std::vector< char > spArePix;
		  // std::vector< char > spAreSCT;
		  // std::vector< long > spLayer;
		  // doDoubStudyReadout(dsOutPars, doubTree, (*spIt), vpTrkNodes, seedTrack, FTFrunItr, z0, 0, doubIsKept);
		  calcDoubStudyParams(dsOutPars, spm, (*spIt), vpTrkNodes, z0, tau, minZ, maxZ, minZ_atSpIt, maxZ_atSpIt, 0, doubIsKept);
		  // std::cout << "CK TEST: After doing doDoubStudyReadout for inner doublets" << std::endl; // CK TEST
		}
		
		  //                if (!m_settings.roiDescriptor->contains(z0, tau)) {
                if (!RoiUtil::contains( *(m_settings.roiDescriptor), z0, tau)) {
                  continue;
                }
              }

              m_SoA.m_sp.push_back(*spIt);
              nSP++;

	      // std::cout << "CK TEST: Finished loop over space points" << std::endl; // CK TEST
            }
          }
        } // Doublets of space points at smaller radius than midpoint

        nInner = nSP;

        //std::cout<<"middle sp : r="<<rm<<" phi="<<spm->phi()<<" z="<<zm<<" has "<<innerSPs.size()<<" inner neighbours"<<std::endl;

        for(int outRadIdx=radIdx+1;(outRadIdx<m_nMaxRadBin) && (nSP<m_maxSoaSize);outRadIdx++) {

          float refRad = m_minRadius +  m_radBinWidth*(0.5+outRadIdx);

          float dR = refRad - rm;

          if(dR<m_minDeltaRadius) continue;
          if(dR>m_maxDeltaRadius) break;

          float maxZ = zMinus + refRad*(zm - zMinus)/rm;
          float minZ = zPlus + refRad*(zm - zPlus)/rm;

          for(auto outPhiIdx : phiVec) {

            const std::vector<const TrigSiSpacePointBase*>& spVec = m_pStore->m_phiSectors[outPhiIdx].m_radBins[outRadIdx];

            if(spVec.empty()) continue;

            std::vector<const TrigSiSpacePointBase*>::const_iterator it1 = std::lower_bound(spVec.begin(), spVec.end(), minZ, PHI_SECTOR::smallerThanZ());
            std::vector<const TrigSiSpacePointBase*>::const_iterator it2 = std::upper_bound(spVec.begin(), spVec.end(), maxZ, PHI_SECTOR::greaterThanZ());

            if(std::distance(it1, it2)==0) continue;

            for(std::vector<const TrigSiSpacePointBase*>::const_iterator spIt= it1; spIt!=it2; ++spIt) {

	      // CK: don't allow doublet formed from space points in the same layer
	      if(spm->layer() == (*spIt)->layer()) continue; // CK

              double tau = (zm - (*spIt)->z())/(rm - (*spIt)->r());
              if (fabs(tau) > 7.41) {
                continue;
              }
              double z0  = zm - rm*tau;

              if (m_settings.m_doubletFilterRZ) {
		//                if (!m_settings.roiDescriptor->contains(z0, tau)) {

		if ( m_settings.roiDescriptor->zed() !=0 && algName.compare("TrigFastTrackFinder_TauIso") == 0 && spMidInTrack) {
		  // std::cout << "CK TEST: Before doing doDoubStudyReadout for outer doublets" << std::endl; // CK TEST
		  float maxZ_atSpIt = zMinus + ( (*spIt)->r() )*(zm - zMinus)/rm;
		  float minZ_atSpIt = zPlus + ( (*spIt)->r() )*(zm - zPlus)/rm;
		  
		  bool doubIsKept = RoiUtil::contains( *(m_settings.roiDescriptor), z0, tau);
		  calcDoubStudyParams(dsOutPars, spm, (*spIt), vpTrkNodes, z0, tau, minZ, maxZ, minZ_atSpIt, maxZ_atSpIt, 1, doubIsKept);
		  // doDoubStudyReadout(dsOutPars, doubTree, (*spIt), vpTrkNodes, seedTrack, FTFrunItr, z0, 1, doubIsKept);
		  // std::cout << "CK TEST: After doing doDoubStudyReadout for outer doublets" << std::endl; // CK TEST
		}
		
                if (!RoiUtil::contains( *(m_settings.roiDescriptor), z0, tau)) {
                  continue;
                }
              }

              if(deIds == (*spIt)->offlineSpacePoint()->elementIdList()) continue;

              m_SoA.m_sp.push_back(*spIt);
              nSP++;
            }
          }
        } // Doublets of space points at larger radius than midpoint

        nOuter = nSP-nInner;

        //std::cout<<"middle sp : r="<<rm<<" phi="<<spm->phi()<<" z="<<zm<<" has "<<outerSPs.size()<<" outer neighbours"<<std::endl;

	// CK tau doublet stuff
	if ( m_settings.roiDescriptor->zed() !=0 && algName.compare("TrigFastTrackFinder_TauIso") == 0) { // CK TEST
	  // std::cout << "CK TEST: Before check prior to doDoubStudyReadout, seedTrack = " << seedTrack << std::endl; // CK TEST
	} // CK TEST
	
	if ( m_settings.roiDescriptor->zed() !=0 && algName.compare("TrigFastTrackFinder_TauIso") == 0 && seedTrack != NULL) {
	  // std::cout << "CK TEST: Passed check, will try to doDoubStudyReadout" << std::endl; // CK TEST
	  doDoubStudyReadout(dsOutPars, doubTree, seedTrack, FTFrunItr);
	  // std::cout << "CK TEST: doDoubStudyReadout finished" << std::endl; // CK TEST
	}
	//

        if(nInner != 0 && nOuter != 0) {
          INTERNAL_TRIPLET_BUFFER tripletMap;

          createTriplets(spm, nInner, nOuter, tripletMap);

          if(!tripletMap.empty()) storeTriplets(tripletMap);	
          tripletMap.clear();
        }
        else {
          m_SoA.m_sp.clear(); 
          continue;
        }


      }//loop over spacepoints in phi-r bin
    }
  }

  /*
  for(auto& tm : m_triplets) {
    std::cout<<"Q="<<tm.first<<" barcodes: "<<tm.second->s1().barCode()<<" "<<
      tm.second->s2().barCode()<<" "<<tm.second->s3().barCode()<<std::endl;
  }
  */
}


void TrigTrackSeedGenerator::createTriplets(const TrigSiSpacePointBase* pS, int nInner, int nOuter,
					    INTERNAL_TRIPLET_BUFFER& output) {

  

  output.clear();
  if(nInner==0 || nOuter==0) return;

  int nSP = nInner + nOuter;

  const double pS_r = pS->r();
  const double pS_x = pS->x();
  const double pS_y = pS->y();
  const double pS_z = pS->z();
  const double pS_dr = pS->dr();
  const double pS_dz = pS->dz();
  const double cosA = pS_x/pS_r;
  const double sinA = pS_y/pS_r;
  const double covZ = pS_dz*pS_dz;
  const double covR = pS_dr*pS_dr;
  const bool isPixel = pS->isPixel();

  m_SoA.resizeComponents();//Resize m_r, mu etc to m_sp size

  for(int idx=0;idx<nSP;idx++) {
    const TrigSiSpacePointBase* pSP = m_SoA.m_sp[idx];
    
    //1. transform in the pS-centric c.s

    const double dx = pSP->x() - pS_x;
    const double dy = pSP->y() - pS_y;
    const double R2inv = 1.0/(dx*dx+dy*dy);
    const double Rinv = sqrt(R2inv);
    const double xn = dx*cosA + dy*sinA;
    const double yn =-dx*sinA + dy*cosA;
    const double dz = ((idx<nInner) - (idx>=nInner)) * (pSP->z() - pS_z);//No-branch version
    //const double dz = (idx<nInner) ? pSP->z() - pS->z() : -pSP->z() + pS->z(); 
    const double t = Rinv*dz;

    //2. Conformal mapping

    m_SoA.m_r[idx] = Rinv;
    m_SoA.m_u[idx] = xn*R2inv;
    m_SoA.m_v[idx] = yn*R2inv;
    
    //3. cot(theta) estimation for the doublet

    const double covZP = pSP->dz()*pSP->dz();
    const double covRP = pSP->dr()*pSP->dr();
    
    m_SoA.m_t[idx] = t;
    m_SoA.m_tCov[idx] = R2inv*(covZ + covZP + t*t*(covR+covRP));
  }


  //double loop

  for(int innIdx=0;innIdx<nInner;innIdx++) {

    //mult. scatt contribution due to the layer with middle SP
    
    const double r_inn = m_SoA.m_r[innIdx];
    const double t_inn = m_SoA.m_t[innIdx];
    const double v_inn = m_SoA.m_v[innIdx];
    const double u_inn = m_SoA.m_u[innIdx];
    const double tCov_inn = m_SoA.m_tCov[innIdx];
    const double dCov = m_CovMS*(1+t_inn*t_inn);

    //double z0 = pS->z() - m_SoA.m_t[innIdx]*pS->r();

    for(int outIdx=nInner;outIdx<nSP;outIdx++) {
      /*
      std::cout<<"Tr: "<<m_SoA.m_sp[innIdx]->offlineSpacePoint()->r()<<" "<<m_SoA.m_sp[innIdx]->offlineSpacePoint()->phi()
	       <<" "<<m_SoA.m_sp[innIdx]->offlineSpacePoint()->globalPosition().z()<<" | ";
      std::cout<<pS->offlineSpacePoint()->r()<<" "<<pS->offlineSpacePoint()->phi()<<" "<<pS->offlineSpacePoint()->globalPosition().z()<<" | ";
      std::cout<<m_SoA.m_sp[outIdx]->offlineSpacePoint()->r()<<" "<<m_SoA.m_sp[outIdx]->offlineSpacePoint()->phi()<<" "
	       <<m_SoA.m_sp[outIdx]->offlineSpacePoint()->globalPosition().z()<<std::endl;
      */

      //1. rz doublet matching
      const double t_out = m_SoA.m_t[outIdx];

      const double dt2 = std::pow((t_inn - t_out), 2)/9.0;

      //const double covdt = tCov_inn + m_SoA.m_tCov[outIdx]
      //                   + 2*r_inn*m_SoA.m_r[outIdx]*(t_inn*t_out*covR + covZ);
      double covdt = (t_inn*t_out*covR + covZ);
      covdt       *= 2*r_inn*m_SoA.m_r[outIdx];
      covdt       += tCov_inn + m_SoA.m_tCov[outIdx];
      //                   + 2*r_inn*m_SoA.m_r[outIdx]*(t_inn*t_out*covR + covZ);
      //const double covdt_old = tCov_inn + m_SoA.m_tCov[outIdx]
      //                   + 2*r_inn*m_SoA.m_r[outIdx]*(t_inn*t_out*covR + covZ);
      //std::cout << std::scientific << "covdt: " << covdt << std::endl;
      //std::cout << std::scientific << "covdt_old: " << covdt_old << std::endl;
      //std::cout << std::endl;
      //double covdt = m_SoA.m_tCov[innIdx] + m_SoA.m_tCov[outIdx];
      //covdt += 2*m_SoA.m_r[innIdx]*m_SoA.m_r[outIdx]*(m_SoA.m_t[innIdx]*m_SoA.m_t[outIdx]*covR + covZ);

      //const double dt2 = dt*dt/9.0;//i.e. 3-sigma cut

      if(dt2 > covdt+dCov) continue;

      //2. pT estimate

      const double du = m_SoA.m_u[outIdx] - u_inn;
      if(du==0.0) continue;
      const double A = (m_SoA.m_v[outIdx] - v_inn)/du;
      const double B = v_inn - A*u_inn;
      const double R_squ = (1 + A*A)/(B*B);

      if(R_squ < m_minR_squ) continue;

      //3. the 3-sigma cut with estimated pT

      const double frac = m_minR_squ/R_squ;
      if(dt2 > covdt+frac*dCov) continue;

      //4. d0 cut

      const double fabs_d0 = std::fabs(pS_r*(B*pS_r - A));

      if(fabs_d0 > m_settings.m_tripletD0Max) continue;
      
      if (m_SoA.m_sp[outIdx]->isSCT() && isPixel) {
        if(fabs_d0 > m_settings.m_tripletD0_PPS_Max) continue;
      }

      //5. phi0 cut

      if (!m_settings.roiDescriptor->isFullscan()) {
        const double uc = 2*B*pS_r - A;
        const double phi0 = atan2(sinA - uc*cosA, cosA + uc*sinA);

        // if(!m_settings.roiDescriptor->containsPhi(phi0)) {
	if ( !RoiUtil::containsPhi( *(m_settings.roiDescriptor), phi0 ) ) {
          continue;
        }
      }

      //6. add new triplet

      const double Q = fabs_d0*fabs_d0;
      if(output.size()>=m_settings.m_maxTripletBufferLength) {
        INTERNAL_TRIPLET_BUFFER::iterator it = output.begin();
        if( Q >= (*it).first) continue;
        output.erase(it);
      }

      TrigInDetTriplet t = TrigInDetTriplet(*m_SoA.m_sp[innIdx], *pS, *m_SoA.m_sp[outIdx], Q);


      output.insert(std::pair<double, TrigInDetTriplet>(Q,t));
    }
  }
  m_SoA.clear();
}


void TrigTrackSeedGenerator::storeTriplets(INTERNAL_TRIPLET_BUFFER& tripletMap) {
  for(INTERNAL_TRIPLET_BUFFER::iterator it=tripletMap.begin();it!=tripletMap.end();++it) {
    double Q = (*it).first;
    if((*it).second.s3().isSCT()) {
      Q += (*it).second.s1().isSCT() ? 1000.0 : 10000.0;
    }
    m_triplets.insert(std::pair<double, TrigInDetTriplet>(Q, (*it).second));
  }
}

void TrigTrackSeedGenerator::getSeeds(std::vector<TrigInDetTriplet>& vs) {
  vs.clear();
  vs.reserve(m_triplets.size());
  for(INTERNAL_TRIPLET_BUFFER::reverse_iterator it=m_triplets.rbegin();it!=m_triplets.rend();++it) {
    vs.push_back((*it).second);
  }
  m_triplets.clear();
}



// CK
// bool matchSpToHit(TrigSiSpacePointBase* sp, Trk::TrkBaseNode* hit) {
//   double deltaZed = 1.;
//   double deltaEta = 0.1;
//   double deltaPhi = 0.1;

//   bool zedMatch = false;
//   bool etaMatch = false;
//   bool phiMatch = false;

//   if (std::abs(sp->z()) - )
// }
